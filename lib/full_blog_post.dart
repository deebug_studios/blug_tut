import 'package:blog_tut/model/blog_post.dart';
import 'package:blog_tut/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'package:blog_tut/auth/login_page.dart';
import 'package:blog_tut/comments.dart';
import 'package:blog_tut/landing_page.dart';

class FullBlogPostPage extends StatefulWidget {

  BlogPost blogPost;
  FullBlogPostPage(this.blogPost);

  @override
  _FullBlogPostPageState createState() => _FullBlogPostPageState();
}

class _FullBlogPostPageState extends State<FullBlogPostPage> {

  Map likes;
  bool isLikedByUser;
  int likesCount;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    likes = widget.blogPost.likes;
    checkLike();
    getLikesCount();
  }

  checkLike(){
    if(likes.containsKey(currentUser.userId)){
      //the user has liked or unliked the post before
      // so isLikedByUser may be true or false
      isLikedByUser = likes[currentUser.userId];

    }else{
      // the user has never liked or unliked the post before
      // so isLikedByUser will definitely be false
      isLikedByUser = false;
    }

  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text(widget.blogPost.title),
      ),
      body: ListView(
        children: [
          Container(
              height: 250,
            child: Image.network(widget.blogPost.postImage, fit: BoxFit.cover)
          ),

          Padding(
            padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
            child: Text(widget.blogPost.title,
                style: TextStyle(fontSize: 18
                )
            )
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            child: Text(timeago.format(
                DateTime.fromMillisecondsSinceEpoch(widget.blogPost.timeStamp)))
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [

                GestureDetector(
                  onTap: (){
                    likeAndUnlikePost();
                  },
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Text('Likes'),
                      SizedBox(width: 8,),
                      isLikedByUser ? Icon(Icons.thumb_up, color: Colors.blue) : Icon(Icons.thumb_up_alt_outlined),
                      SizedBox(width: 8,),
                      Text(likesCount == null ? '...' : '$likesCount')
                    ],
                  ),
                ),

                SizedBox(width: 20,),

                GestureDetector(
                    onTap: (){

                      if(currentUser == null){
                        Navigator.of(context).push(
                            CupertinoPageRoute(builder: (_)=> LoginPage()));
                      }else{
                        goToCommentsPage(widget.blogPost.postId);
                      }

                    },
                    child: Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      children: [
                        Text('Comments'),
                        SizedBox(width: 8,),
                        Icon(Icons.comment),
                        SizedBox(width: 8,),
                        getCommentsCount()
                      ],
                    )
                )
              ],
            )
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Text(widget.blogPost.body)
          )
        ],
      )
    );
  }

  likeAndUnlikePost(){

    String userId = currentUser.userId;

    if(isLikedByUser){
      // unlike post
      FirebaseFirestore.instance.collection(Constants.BLOG_POSTS_COLLECTION)
          .doc(widget.blogPost.postId)
          .update({
        'likes.$userId': false
      }).then((_){
        setState(() {
          isLikedByUser = false;
          likesCount--;
        });
      });

      // remove from likes collection
      FirebaseFirestore.instance.collection('post_likes')
          .doc(widget.blogPost.postId)
          .collection(widget.blogPost.postId)
          .doc(widget.blogPost.postId)
          .delete();


    }else{
      // like a post
      FirebaseFirestore.instance.collection(Constants.BLOG_POSTS_COLLECTION)
          .doc(widget.blogPost.postId)
          .update({
        'likes.$userId': true
      }).then((_){
        setState(() {
          isLikedByUser = true;
          likesCount++;
        });
      });

      // add to likes collection
      FirebaseFirestore.instance.collection('post_likes')
          .doc(widget.blogPost.postId)
          .collection(widget.blogPost.postId)
          .doc(widget.blogPost.postId)
          .set({
        'liker_id': currentUser.userId
      });


    }



  }

  getLikesCount(){
    FirebaseFirestore.instance.collection('post_likes')
        .doc(widget.blogPost.postId)
        .collection(widget.blogPost.postId)
        .get().then((QuerySnapshot snap){
          setState(() {
            likesCount = snap.docs.length;
          });
    });
  }

  getCommentsCount(){
    return FutureBuilder(
      future: FirebaseFirestore.instance.collection('comments')
          .doc(widget.blogPost.postId)
          .collection(widget.blogPost.postId).get(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snap){
        if(!snap.hasData){
          return Text('...');
        }

        return Text('${snap.data.docs.length}');
      },
    );
  }

  // function to be called when the user taps the comment button
  // it'll take the user to the comments page
  goToCommentsPage(String postId){
    Navigator.of(context).push(
        CupertinoPageRoute(builder: (_)=> CommentsPage(postId)));
  }

}
