import 'package:blog_tut/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:blog_tut/landing_page.dart';
import 'package:firebase_auth/firebase_auth.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  // check if user is logged in
  User user = FirebaseAuth.instance.currentUser;

  if(user == null) {
    runApp(MyApp(false, null));
  } else {
    FirebaseFirestore.instance.collection(Constants.USERS_COLLECTION)
        .doc(user.uid).get().then((DocumentSnapshot docUser){
          runApp(MyApp(true, docUser));
    });
  }
}

class MyApp extends StatelessWidget {

  bool isLoggedIn;
  DocumentSnapshot userDoc;

  MyApp(this.isLoggedIn, this.userDoc);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Blog App',
      theme: ThemeData(

        primarySwatch: Colors.amber,
      ),
      home: LandingPage(isLoggedIn: isLoggedIn, userDoc: userDoc)
    );
  }
}
