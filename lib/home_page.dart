import 'package:blog_tut/model/blog_post.dart';
import 'package:blog_tut/utils/constants.dart';
import 'package:blog_tut/widgets/blog_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: buildBlogPosts(),
    );
  }

  buildBlogPosts(){
    return StreamBuilder(
      stream: FirebaseFirestore.instance.collection(Constants.BLOG_POSTS_COLLECTION)
          .orderBy(Constants.TIME_STAMP, descending: true).snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snap){
        if(!snap.hasData){
          return Container(
            height: 100,
            width: 100,
            child: CircularProgressIndicator()
          );
        }

        return ListView.builder(
          itemCount: snap.data.docs.length,
          itemBuilder: (BuildContext context, int index){

            DocumentSnapshot blogPostSnapshotItem = snap.data.docs[index];
            BlogPost currentBlogPostItem = BlogPost.fromSnapshot(blogPostSnapshotItem);
            return BlogItem(currentBlogPostItem);

          },
        );
      },
    );
  }
}
