import 'package:blog_tut/landing_page.dart';
import 'package:blog_tut/model/blog_post.dart';
import 'package:blog_tut/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class AddEditBlogPost extends StatefulWidget {
  BlogPost blogPost;
  AddEditBlogPost({this.blogPost});

  @override
  _AddEditBlogPostState createState() => _AddEditBlogPostState(blogPost);
}

class _AddEditBlogPostState extends State<AddEditBlogPost> {
  BlogPost blogPost;
  _AddEditBlogPostState(this.blogPost);

  // text editing controllers
  TextEditingController titleController = TextEditingController();
  TextEditingController bodyController = TextEditingController();

  bool shouldEdit = false;
  String editImageUrl = '';
  @override
  void initState() {
    super.initState();

    if (blogPost != null) {
      // blog is to be edited
      shouldEdit = true;

      titleController.text = blogPost.title;
      bodyController.text = blogPost.body;

      for (int i = 0; i < blogPost.tags.length; i++) {
        // populate List of strings
        tagsForDb.add(blogPost.tags[i]);
        // populate list of widgets

        tagWidgetItems.add(Padding(
            padding: const EdgeInsets.only(right: 6),
            child: Chip(
              label: Text(blogPost.tags[i]),
              backgroundColor: Colors.yellow,
            )));
      }

      editImageUrl = blogPost.postImage;
    }
  }

  // List of chips
  List<Padding> tagWidgetItems = [];
  // List of Strings for db
  List<String> tagsForDb = [];
  // variable to hold single tag
  String tempTag;

  // controller for tag textfield
  TextEditingController tagController = TextEditingController();

  addTag() {
    if (tempTag == null || tempTag == '') {
      // warn user
    } else {
      // add tag to list of tags
      tagsForDb.add(tempTag);

      Padding tagChip = Padding(
          padding: const EdgeInsets.only(right: 6),
          child: Chip(
            label: Text(tempTag),
            backgroundColor: Colors.yellow,
          ));
      tagWidgetItems.add(tagChip);
      tagController.text = '';
    }

    setState(() {});
  }

  removeTag() {
    if (tagsForDb.length >= 1) {
      //tagsForDb.removeAt(tagsForDb.length - 1); // remove last item in list
      tagsForDb.removeLast();
      tagWidgetItems.removeLast();

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            title: Text(shouldEdit ? 'Edit Blog Post' : 'Add Blog Post'),
            actions: [
              IconButton(
                  icon: Icon(Icons.check),
                  onPressed: () {
                    postBlogPost();
                  }),
            ]),
        body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Stack(
              children: [
                SingleChildScrollView(
                    child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Text('Title', style: TextStyle(fontSize: 18))),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.black12,
                        ),
                        child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: TextField(
                              enabled: !postIsUploading,
                              controller: titleController,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Enter Post Title'),
                            ))),
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Text('Body', style: TextStyle(fontSize: 18))),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.black12,
                        ),
                        child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: TextField(
                              enabled: !postIsUploading,
                              controller: bodyController,
                              minLines: 10,
                              maxLines: 10,
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Enter Post Body'),
                            ))),
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Text('Tags', style: TextStyle(fontSize: 18))),
                    SizedBox(
                      height: 10,
                    ),
                    Wrap(children: tagWidgetItems),
                    Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.black12,
                        ),
                        child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: TextField(
                              enabled: !postIsUploading,
                              controller: tagController,
                              onChanged: (val) {
                                tempTag = val;
                              },
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Enter Tag',
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      Icons.add_circle_outline,
                                      color: Colors.green,
                                    ),
                                    onPressed: () {
                                      addTag();
                                    },
                                  ),
                                  prefixIcon: IconButton(
                                      icon: Icon(Icons.remove_circle_outline,
                                          color: Colors.red),
                                      onPressed: () {
                                        removeTag();
                                      })),
                            ))),
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                        alignment: Alignment.topLeft,
                        child: Text('Upload Image',
                            style: TextStyle(fontSize: 18))),
                    SizedBox(
                      height: 10,
                    ),
                    Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                            height: 100,
                            width: 100,
                            color: Colors.black12,
                            child: _image == null
                                ? shouldEdit
                                    ? Image.network(editImageUrl)
                                    : Container(
                                        height: 0,
                                        width: 0,
                                      )
                                : Image.file(_image))),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ElevatedButton(
                          child: Text('Gallery', style: TextStyle(
                            color: Colors.white
                          ),),
                          style: ElevatedButton.styleFrom(primary: Colors.blue),
                          onPressed: postIsUploading ? null : () {
                                  getImage(false);
                                },
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        ElevatedButton(
                          child: Text('Camera', style: TextStyle(
                            color: Colors.white
                          ),),
                          style: ElevatedButton.styleFrom(primary: Colors.blue),
                          onPressed: postIsUploading ? null : () {
                                  getImage(true);
                                },
                        )
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                )),
                Align(
                    alignment: Alignment.center,
                    child: Container(
                        width: 100,
                        height: 100,
                        child: Visibility(
                          child: CircularProgressIndicator(),
                          visible: postIsUploading,
                        )))
              ],
            )));
  }

  // function to get image as a file
  File _image;
  final picker = ImagePicker();
  Future getImage(bool fromCamera) async {
    final pickedFile = await picker.getImage(
        source: fromCamera ? ImageSource.camera : ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  // function to post the blog post to the db
  bool postIsUploading = false;
  postBlogPost() {
    setState(() {
      postIsUploading = true;
    });

    if (titleController.text == null || titleController.text == '') {
      print('Title cannot be empty');
      setState(() {
        postIsUploading = false;
      });
      return;
    }

    if (bodyController.text == null || bodyController.text == '') {
      print('Body cannot be empty');
      setState(() {
        postIsUploading = false;
      });
      return;
    }

    if (_image == null && editImageUrl == '') {
      print('You must upload an image');
      setState(() {
        postIsUploading = false;
      });
      return;
    }

    storeBlogDataToFirestore();
  }

  //
  storeImageToFirebaseStorage(String postId) {
    // generate a unique file name for the image
    // get a reference to the storage location for the image
    final Reference storageReference = FirebaseStorage.instance
        .ref()
        .child("blog_post_images/$postId.jpg");
    // store the image to firebase storage
    storageReference
        .putFile(_image)
        .onComplete
        .then((StorageTaskSnapshot taskSnapshot) {
      // get a link to the image location

      storageReference.putFile(_image).whenComplete((){

      });
      taskSnapshot.ref.getDownloadURL().then((downloadUrl) {
        // at this point we have the download url for the image
        // update blog data in firestore
        FirebaseFirestore.instance
            .collection(Constants.BLOG_POSTS_COLLECTION)
            .doc(postId)
            .update({
          Constants.POST_IMAGE: downloadUrl,
          Constants.POST_IMAGE_THUMB: downloadUrl
        }).then((_) {
          // pop page
          setState(() {
            postIsUploading = false;
          });
          Navigator.of(context).pop();
        }).catchError((e) {
          print(e);
          setState(() {
            postIsUploading = false;
          });
        });
      }).catchError((e) {
        print(e);
        setState(() {
          postIsUploading = false;
        });
      });
    }).catchError((e) {
      print(e);
      setState(() {
        postIsUploading = false;
      });
    });
  }

  bool imageChanged = false;
  storeBlogDataToFirestore() {

    if(shouldEdit){

      if(_image == null){
        // image is not changing
        imageChanged = false;
      }else{
        imageChanged = true;
      }

      // update the data in the database
      FirebaseFirestore.instance.collection(Constants.BLOG_POSTS_COLLECTION)
      .doc(blogPost.postId)
      .update({
        Constants.TITLE: titleController.text,
        Constants.BODY: bodyController.text,
        Constants.TAGS: tagsForDb,
        Constants.POST_IMAGE: imageChanged ? '' : editImageUrl,
        Constants.POST_IMAGE_THUMB: imageChanged ? '' : editImageUrl
      }).then((_){

        if(imageChanged){
          storeImageToFirebaseStorage(blogPost.postId);
        }else{
          // pop page
          setState(() {
            postIsUploading = false;
          });
          Navigator.of(context).pop();
        }

      }).catchError((e){
        print(e);
        setState(() {
          postIsUploading = false;
        });
      });

    }else{
      // store data normally to firestore
      int timeStamp = DateTime.now().millisecondsSinceEpoch;
      BlogPost newPost = BlogPost(
          title: titleController.text,
          body: bodyController.text,
          posterId: currentUser.userId,
          postId: timeStamp.toString(),
          likes: {},
          timeStamp: timeStamp,
          postImageThumb: '',
          postImage: '',
          tags: tagsForDb);

      FirebaseFirestore.instance
          .collection(Constants.BLOG_POSTS_COLLECTION)
          .doc(timeStamp.toString())
          .set(newPost.toMap())
          .then((_) {
        // upload the post Image
        storeImageToFirebaseStorage(timeStamp.toString());
      }).catchError((e) {
        print(e);
        setState(() {
          postIsUploading = false;
        });
      });
    }

  }
}
