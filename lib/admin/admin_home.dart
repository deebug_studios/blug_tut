import 'package:blog_tut/model/blog_post.dart';
import 'package:blog_tut/utils/constants.dart';
import 'package:blog_tut/utils/utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:blog_tut/admin/add_edit_blog_post.dart';

class AdminHome extends StatefulWidget {
  @override
  _AdminHomeState createState() => _AdminHomeState();
}

class _AdminHomeState extends State<AdminHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Admin Console'),
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
      ),
      body: getAllBlogPosts(),
      floatingActionButton: FloatingActionButton.extended(
        isExtended: true,
        label: Text('New Blog Post'),
        icon: Icon(Icons.add),
        onPressed: (){
          Navigator.of(context).push(
            CupertinoPageRoute(builder: (_)=> AddEditBlogPost()));
        },
      ),
    );
  }

  // function to return all blog posts
  getAllBlogPosts(){
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection(Constants.BLOG_POSTS_COLLECTION)
      .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snap){
        if(!snap.hasData){
          return Container(
            height: 100,
            width: 100,
            child: CircularProgressIndicator()
          );
        }

        return ListView.builder(
          itemCount: snap.data.docs.length,
          itemBuilder: (BuildContext context, int index){

            DocumentSnapshot blogPostSnapshotItem = snap.data.docs[index];
            BlogPost currentBlogPostItem = BlogPost.fromSnapshot(blogPostSnapshotItem);
            String title = currentBlogPostItem.title;

            return Padding(
              padding: const EdgeInsets.only(top: 10),
              child: ListTile(
                  title: Text(title),
                  subtitle: Text('${currentBlogPostItem.body} \n${MyUtils.parseTime(currentBlogPostItem.timeStamp)}'),
                  trailing: Container(
                      height: 100,
                      width: 100,
                      child: ClipRect(
                        child: currentBlogPostItem.postImageThumb == '' ? Container(color: Colors.grey,) : Image.network(currentBlogPostItem.postImageThumb, fit: BoxFit.cover,),
                      )
                  ),
                onTap: (){
                    Navigator.of(context).push(
                        CupertinoPageRoute(builder: (_)=> AddEditBlogPost(blogPost: currentBlogPostItem,)));
                },
              )
            );
          },
        );
      }
    );
  }


}
