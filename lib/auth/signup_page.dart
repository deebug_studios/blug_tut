import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../utils/constants.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  String email;
  String password;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

//    String NAME = 'name';
//    String AGE = 'age';
//    String OCCUPATION = 'occupation';
//    String SALARY = 'salary';
//
//    Map<String, dynamic> person = {
//      'name': 'John',
//      'age': 67,
//      'occupation': 'Programmer',
//      'salary': 10000000000.5
//    };
//
//
//    print(person[AGE]);
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: [
            SizedBox(
              height: 100,
            ),
            Align(
                alignment: Alignment.topCenter,
                child: Image.asset(
                  'assets/logo.png',
                  scale: 5,
                )),
            SizedBox(
              height: 50,
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Email',
                        suffixIcon: Icon(Icons.email, color: Colors.green)),
                    keyboardType: TextInputType.emailAddress,
                    onChanged: (String val) {
                      email = val;
                    })),
            SizedBox(
              height: 16,
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Password',
                        suffixIcon: Icon(Icons.lock, color: Colors.green)),
                    onChanged: (String val) {
                      password = val;
                    })),
            SizedBox(
              height: 30,
            ),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: MaterialButton(
                  color: Colors.yellow,
                  child: isLoading ? Container(
                    height: 24,
                    width: 24,
                    child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                      strokeWidth: 2,

                    )
                  ) : Text('Sign UP'.toUpperCase(),
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  onPressed: () {
                    checkUserInput();
                  },
                ))
          ],
        ));
  }



  // function to check the user's entry
  checkUserInput(){
    isLoading = true;
    setState(() {});

    if(email == null  || email.isEmpty){
      print('Please enter an email');
      isLoading = false;
      setState(() {});
      return;
    }

    if( password == null || password.isEmpty){
      print('please enter a password');
      isLoading = false;
      setState(() {});
      return;
    }

    // sign the user up
    signUpUserWithFirebase();
  }

  // actual sign up function using firebase
  bool isLoading = false;
  signUpUserWithFirebase(){

    FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: password
    ).then((UserCredential credential){
      // store user data to firestore
      storeUserDataToFirestore(credential.user.uid);
    }).catchError((e){
      print(e);
      isLoading = false;
      setState(() {});
    });

  }

  storeUserDataToFirestore(String userId){
    Map<String, dynamic> userData = Map<String, dynamic>();
    userData = {
      Constants.USERNAME: '@username',
      Constants.USER_ID: userId,
      Constants.EMAIL: email,
      Constants.IS_ADMIN: false,
      Constants.PROFILE_IMAGE: '',
      Constants.PROFILE_IMAGE_THUMB: ''
    };

    CollectionReference usersRef = FirebaseFirestore.instance.collection(Constants.USERS_COLLECTION);
    usersRef.doc(userId)
    .set(userData).then((_){
      // DO SOMETHING WHEN THE PROCESS COMPLETES
      isLoading = false;
      setState(() {});
    }).catchError((e){
      print(e);
    });
  }

}
