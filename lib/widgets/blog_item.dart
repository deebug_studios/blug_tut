import 'package:blog_tut/auth/login_page.dart';
import 'package:blog_tut/landing_page.dart';
import 'package:blog_tut/model/blog_post.dart';
import 'package:blog_tut/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'package:blog_tut/comments.dart';
import 'package:blog_tut/full_blog_post.dart';

class BlogItem extends StatefulWidget {

  BlogPost blogPost;

  BlogItem(this.blogPost);


  @override
  _BlogItemState createState() => _BlogItemState();
}

class _BlogItemState extends State<BlogItem> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(
            CupertinoPageRoute(builder: (_)=> FullBlogPostPage(widget.blogPost)));
      },
      child: Padding(
          padding: EdgeInsets.only(top: 16, left: 8, right: 8),
          child: Container(
              
              height: 200,
              child: Material(
                color: Colors.black12,
                borderRadius: BorderRadius.circular(16),
                child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(widget.blogPost.title,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400
                              )
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Padding(
                                    padding: const EdgeInsets.only(right: 6),
                                    child: Column(
                                      children: [
                                        Align(
                                            alignment: Alignment.topCenter,
                                            child: Text(widget.blogPost.body, style: TextStyle())
                                        ),

                                        Align(
                                          alignment: Alignment.bottomLeft,
                                          child: Padding(
                                              padding: const EdgeInsets.only(top: 10),
                                              child: Text(timeago.format(DateTime.fromMillisecondsSinceEpoch(widget.blogPost.timeStamp)))
                                          ),
                                        )
                                      ],
                                    )
                                ),
                              ),

                              Container(
                                  height: 120,
                                  width: 150,
                                  child: ClipRRect(
                                      borderRadius: BorderRadius.circular(16),
                                      child: Image.network(widget.blogPost.postImageThumb, fit: BoxFit.fill,)
                                  )
                              )
                            ],
                          ),
                        ),

                        Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [

                                Wrap(
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  children: [
                                    Text('Likes'),
                                    SizedBox(width: 8,),
                                    checkLiked(widget.blogPost.likes) ? Icon(Icons.thumb_up, color: Colors.blue,) : Icon(Icons.thumb_up_alt_outlined),
                                    SizedBox(width: 8,),
                                    getLikesCount()
                                  ],
                                ),

                                GestureDetector(
                                    onTap: (){

                                      if(currentUser == null){
                                        Navigator.of(context).push(CupertinoPageRoute(builder: (_)=> LoginPage()));
                                      }else{
                                        goToCommentsPage(widget.blogPost.postId);
                                      }

                                    },
                                    child: Wrap(
                                      crossAxisAlignment: WrapCrossAlignment.center,
                                      children: [
                                        Text('Comments'),
                                        SizedBox(width: 8,),
                                        Icon(Icons.comment),
                                        SizedBox(width: 8,),
                                        getCommentsCount()
                                      ],
                                    )
                                )
                              ],
                            )
                        )
                      ],
                    )
                ),
              )
          )
      )
    );
  }

  getCommentsCount(){
    return FutureBuilder(
      future: FirebaseFirestore.instance.collection('comments').doc(widget.blogPost.postId)
      .collection(widget.blogPost.postId).get(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snap){
        if(!snap.hasData){
          return Text('...');
        }

        return Text('${snap.data.docs.length}');
      },
    );
  }

  getLikesCount(){
    return FutureBuilder(
      future: FirebaseFirestore.instance.collection('post_likes')
          .doc(widget.blogPost.postId)
          .collection(widget.blogPost.postId)
    .get(),
    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snap){
        if(!snap.hasData){
          return Text('...');
        }

        return Text('${snap.data.docs.length}');
    },

    );

  }

  bool checkLiked(Map likes){
    if(likes.containsKey(currentUser.userId)){
      return likes[currentUser.userId];
    }else{
      return false;
    }
  }


  // function to be called when the user taps the comment button
  // it'll take the user to the comments page
  goToCommentsPage(String postId){
    Navigator.of(context).push(
        CupertinoPageRoute(builder: (_)=> CommentsPage(postId))).then((val){
      setState(() {

      });
    });
  }

}
