class Constants{
  static const String USERS_COLLECTION = 'users';

  // User doc fields
  static const String USERNAME = 'username';
  static const String EMAIL = 'email';
  static const String USER_ID = 'user_id';
  static const String IS_ADMIN = 'is_admin';
  static const String PROFILE_IMAGE = 'profile_image';
  static const String PROFILE_IMAGE_THUMB = 'profile_image_thumb';


  // blog post doc fields
  static const String BLOG_POSTS_COLLECTION = 'blog_posts';

  static const String POST_ID = 'post_id';
  static const String TITLE = 'title';
  static const String BODY = 'body';
  static const String TIME_STAMP = 'timestamp';
  static const String POST_IMAGE = 'post_image';
  static const String POST_IMAGE_THUMB = 'post_image_thumb';
  static const String POSTER_ID = 'poster_id';
  static const String LIKES = 'likes';
  static const String TAGS = 'tags';

}