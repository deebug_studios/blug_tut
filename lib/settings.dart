import 'package:blog_tut/admin/admin_home.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:blog_tut/auth/login_page.dart';
import 'package:blog_tut/landing_page.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  bool userIsNotLoggedIn = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(currentUser == null){
      userIsNotLoggedIn = true;
    }else{
      userIsNotLoggedIn = false;
    }
  }

  Widget buildProfileImage(){
    if(currentUser.profileImage == ''){
      return CircleAvatar(
        backgroundColor: Colors.black,
        child: CircleAvatar(
          backgroundColor: Colors.white,
          radius: 19,
          child: Icon(Icons.person, size: 30,)
        ),
      );
    }else{
      return CircleAvatar(
        backgroundImage: NetworkImage(currentUser.profileImage),
      );
    }
  }

  bool shouldShowAdminTile(){
    if(userIsNotLoggedIn){
      return false;
    }else{
      if(currentUser.isAdmin){
        return true;
      }else{
        return false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ListTile(
            title: Text(userIsNotLoggedIn ? 'Guest' : currentUser.username),
            subtitle: Text(userIsNotLoggedIn ? 'Sign Up/Login' : 'Edit Profile'),
            leading: userIsNotLoggedIn ? Icon(Icons.person, size: 40,) : buildProfileImage(),
            onTap: (){
              if(userIsNotLoggedIn){
                // navigate to login page
                Navigator.of(context).push(CupertinoPageRoute(builder: (_)=> LoginPage()));
              }else{
                // navigate to profile edit page
              }
            },
          ),

          shouldShowAdminTile() ? ListTile(
            title: Text('Admin Control Panel'),
            subtitle: Text('Blogger'),
            leading: Icon(Icons.dashboard),
            onTap: (){
              // navigate to admin control panel
              Navigator.of(context).push(
                CupertinoPageRoute(
                  builder: (_) => AdminHome()
                )
              );
            },
          ): Container(
            height: 0,
            width: 0,
          ),

          ListTile(
            leading: Icon(Icons.account_box),
            title: Text('Logout'),
            subtitle: Text('Logout of your account'),
            onTap: (){
              if(userIsNotLoggedIn){
                // go to login page
              }else{
                showWarningDialog();
              }
            },
          )

        ],
      )
    );
  }


  showWarningDialog(){
    showDialog(
      context: context,
      builder: (BuildContext ctx){
        return AlertDialog(
          title: Text('Logout?'),
          content: Text('Are you sure you want to log out?'),
          actions: [
            TextButton(
              child: Text('Yes'),
              onPressed: (){
                // pop dialog
                // logout
                FirebaseAuth.instance.signOut().then((_){
                  userIsNotLoggedIn = true;
                  Navigator.of(ctx).pop();
                  currentUser = null;
                  setState(() {

                  });
                });

              },
            ),

            TextButton(
              child: Text('No'),
              onPressed: (){
                // pop dialog
                Navigator.of(ctx).pop();
              },
            )
          ]
        );
      }
    );
  }
}
