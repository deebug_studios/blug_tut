import 'package:blog_tut/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class User{
  String username;
  String email;
  String userId;
  bool isAdmin;
  String profileImage;
  String profileImageThumb;

  User({this.username,
    this.email,
    this.userId,
    this.isAdmin,
    this.profileImage,
    this.profileImageThumb});

  factory User.fromSnapshot(DocumentSnapshot doc){
    return User(
      username: doc.data()[Constants.USERNAME],
      email: doc.data()[Constants.EMAIL],
      userId: doc.data()[Constants.USER_ID],
      isAdmin: doc.data()[Constants.IS_ADMIN],
      profileImage: doc.data()[Constants.PROFILE_IMAGE],
      profileImageThumb: doc.data()[Constants.PROFILE_IMAGE_THUMB]
    );
  }



}