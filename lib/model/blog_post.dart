import 'package:blog_tut/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class BlogPost{

  final String postId;
  final String posterId;
  final String title;
  final String body;
  final List<dynamic> tags;
  final String postImage;
  final String postImageThumb;
  final int timeStamp;
  final Map likes;

  BlogPost({
    this.postId,
    this.posterId,
    this.title,
    this.body,
    this.tags,
    this.postImage,
    this.postImageThumb,
    this.timeStamp,
    this.likes
  });


  factory BlogPost.fromSnapshot(DocumentSnapshot documentSnapshot){
    return BlogPost(
      postId: documentSnapshot.data()[Constants.POST_ID],
      posterId: documentSnapshot.data()[Constants.POSTER_ID],
      title: documentSnapshot.data()[Constants.TITLE],
      body: documentSnapshot.data()[Constants.BODY],
      tags: documentSnapshot.data()[Constants.TAGS],
      postImage: documentSnapshot.data()[Constants.POST_IMAGE],
      postImageThumb: documentSnapshot.data()[Constants.POST_IMAGE_THUMB],
      timeStamp: documentSnapshot.data()[Constants.TIME_STAMP],
      likes: documentSnapshot.data()[Constants.LIKES]
    );
  }

  Map<String, dynamic> toMap(){

    Map<String, dynamic> map = {
      Constants.POST_ID: this.postId,
      Constants.POSTER_ID: this.posterId,
      Constants.TITLE: this.title,
      Constants.BODY: this.body,
      Constants.TAGS: this.tags,
      Constants.POST_IMAGE: this.postImage,
      Constants.POST_IMAGE_THUMB: this.postImageThumb,
      Constants.TIME_STAMP: this.timeStamp,
      Constants.LIKES: this.likes

    };

    return map;
  }
}