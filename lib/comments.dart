import 'package:blog_tut/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'landing_page.dart';
import 'package:timeago/timeago.dart' as timeago;

class CommentsPage extends StatefulWidget {
  String postId;

  CommentsPage(this.postId);

  @override
  _CommentsPageState createState() => _CommentsPageState(postId);
}

class _CommentsPageState extends State<CommentsPage> {
  String postId;
  _CommentsPageState(this.postId);

  TextEditingController commentEditingController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    commentEditingController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Comments'),
          automaticallyImplyLeading: false,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.of(context).pop("my value");
            },
          )
        ),
        body: Column(
          children: [
            Expanded(child: fetchComments()),
            Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                child: Container(
                    height: 50,
                    child: Row(
                      children: [
                        Expanded(
                          child: TextField(
                            controller: commentEditingController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Add Comment',
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 6),
                          child: CircleAvatar(
                              backgroundColor: Colors.yellow,
                              child: IconButton(
                                icon: Icon(
                                  Icons.send,
                                  color: Colors.black,
                                ),
                                onPressed: () {
                                  addComment();
                                },
                              )),
                        )
                      ],
                    )))
          ],
        ));
  }

  fetchComments() {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('comments')
          .doc(postId)
          .collection(postId)
          .orderBy('timestamp', descending: true)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snap) {
        if (!snap.hasData) {
          return Container(
              height: 100, width: 100, child: CircularProgressIndicator());
        }

        return ListView.builder(
          itemCount: snap.data.docs.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: buildUserImage(snap.data.docs[index].data()['commenter_id']),
              title: buildUsername(snap.data.docs[index].data()['commenter_id']),
              subtitle: Text(
                  '''${snap.data.docs[index].data()['comment']}\n\n${timeago.format(
                      DateTime.fromMillisecondsSinceEpoch(snap.data.docs[index].data()['timestamp']))}'''),
            );
          },
        );
      },
    );
  }

  buildUserImage(String commenterId){
    return FutureBuilder(
      future: FirebaseFirestore.instance.collection(Constants.USERS_COLLECTION)
      .doc(commenterId).get(),
      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snap){
        if(!snap.hasData){
          return Container(
            height: 30,
            width: 30,
            child: CircularProgressIndicator()
          );
        }
        print(snap.data[Constants.PROFILE_IMAGE]);
        return snap.data[Constants.PROFILE_IMAGE] == ''
            ? CircleAvatar(child: Icon(Icons.person),)
            : CircleAvatar(backgroundImage: NetworkImage(snap.data[Constants.PROFILE_IMAGE]),);
      },
    );
  }

  buildUsername(String commenterId){
    return FutureBuilder(
      future: FirebaseFirestore.instance
          .collection(Constants.USERS_COLLECTION)
          .doc(commenterId).get(),
      builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> snap){

        if(!snap.hasData){
          return LinearProgressIndicator();
        }

        return Text(snap.data['username']);
      },
    );
  }

  addComment() {
    // handle error checks
    if ((commentEditingController.text == '') ||
        (commentEditingController.text == null)) {
      return;
    }

    String comment = commentEditingController.text;
    commentEditingController.text = '';

    int timeStamp = DateTime.now().millisecondsSinceEpoch;

    FirebaseFirestore.instance
        .collection('comments')
        .doc(postId)
        .collection(postId)
        .doc(timeStamp.toString())
        .set({
      'comment': comment,
      'timestamp': timeStamp,
      'commenter_id': currentUser.userId
    }).catchError((e) {
      print(e);
    });
  }
}
