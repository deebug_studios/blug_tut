import 'dart:io';
import 'package:blog_tut/model/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:blog_tut/home_page.dart';
import 'package:blog_tut/settings.dart';


User currentUser;

class LandingPage extends StatefulWidget {

  bool isLoggedIn;
  DocumentSnapshot userDoc;

  LandingPage({this.isLoggedIn, this.userDoc});

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {


  int pageIndex = 0;

  displayPage(){
    if(pageIndex == 0){
      return HomePage();
    }

    if(pageIndex == 1){
      return SettingsPage();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.isLoggedIn){
      currentUser = User.fromSnapshot(widget.userDoc);
    }else{
      currentUser = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(pageIndex == 0 ? 'Home' : 'Settings'),
      ),
      body: displayPage(),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home'
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings'
          )
        ],
        onTap: (int pageIndex){
          setState(() {
            this.pageIndex = pageIndex;
          });
        },
        currentIndex: pageIndex,
      ),
    );
  }
}


